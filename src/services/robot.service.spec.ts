import { TestBed } from '@angular/core/testing';

import { ToyRobotService } from './toy-robot.service';

describe('RobotService', () => {
  let service: ToyRobotService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ToyRobotService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
