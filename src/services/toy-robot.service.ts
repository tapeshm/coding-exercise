import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ToyRobotService {

    public x: number = null;
    public y: number = null;
    public f: string = null;

    //default values to set while testing the app in DEV mode to avoid retyping everytime in the textbox
    // public x: number = 0;
    // public y: number = 0;
    // public f: string = 'N';

    //clock wise array of directions
    private readonly directions: string[] = ['N', 'E', 'S', 'W'];
    public logs: string[] = [];

    constructor() { }

    //turns the robot left or right
    private turn(number) {
        let index = this.directions.indexOf(this.f.toUpperCase());
        index = index + number;

        if (index >= this.directions.length)
            index = 0;
        else if (index < 0)
            index = this.directions.length - 1;

        return index;
    }

    //tried to advances the robot to next position which may be a valid one or invalid
    private advance(): number[] {
        switch (this.f.toUpperCase()) {
            case 'N':
                return [this.x, this.y + 1];
                break;
            case 'E':
                return [this.x + 1, this.y];
                break;
            case 'S':
                return [this.x, this.y - 1];
                break;
            case 'W':
                return [this.x - 1, this.y];
                break;
        }
    }

    //track if the robot is placed if not we hide the MOVE, LEFT, RIGHT and REPORT buttons
    isPlaced() {
        return this.x !== null && this.y !== null && this.f !== null;
    }

    performAction(command) {

        switch (command) {
            case 'LEFT':
            case 'RIGHT':
                let index = this.turn(command == 'LEFT' ? -1 : 1);
                this.f = this.directions[index];
                this.logs.splice(0, 0, `Current position of robot at X: ${this.x}, Y: ${this.y}, D: ${this.f}`);
                break;
            case 'MOVE':
                let newPosition = this.advance();
                if(newPosition[0] < 0 || newPosition[0] >= 5 || newPosition[1] < 0 || newPosition[1] >= 5){
                    this.logs.splice(0, 0, `Robot will fall off the table if we place it at: ${newPosition[0]}, Y: ${newPosition[1]}, D: ${this.f}`);
                }
                else{
                    this.x = newPosition[0];
                    this.y = newPosition[1];
                    this.logs.splice(0, 0, `Current position of robot at X: ${this.x}, Y: ${this.y}, D: ${this.f}`);
                }
                break;
        }

    }

}
