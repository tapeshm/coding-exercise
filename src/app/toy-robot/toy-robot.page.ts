import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { ToyRobotService } from 'src/services/toy-robot.service';

@Component({
    selector: 'app-tab1',
    templateUrl: 'toy-robot.page.html',
    styleUrls: ['toy-robot.page.scss']
})
export class ToyRobotPage implements OnInit {

    robotForm: FormGroup;

    constructor(private alertController: AlertController, public toyRobotService: ToyRobotService, private formBuilder: FormBuilder) {
    }

    ngOnInit() {

        //used for validating the form
        this.robotForm = this.formBuilder.group({
            x: new FormControl(this.toyRobotService.x, [
                Validators.required,
                Validators.min(0),
                Validators.max(4)
            ]),
            y: new FormControl(this.toyRobotService.y, [
                Validators.required,
                Validators.min(0),
                Validators.max(4)
            ]),
            f: new FormControl(this.toyRobotService.f, [
                Validators.required,
                Validators.pattern(/^(N|S|E|W)$/i),
            ]),
        });
    }

    get x() { return this.robotForm.get('x'); }
    get y() { return this.robotForm.get('y'); }
    get f() { return this.robotForm.get('f'); }

    //place the robot at a particular square and direction
    place() {
        if (this.robotForm.valid) {
            this.toyRobotService.x = this.robotForm.get('x').value;
            this.toyRobotService.y = this.robotForm.get('y').value;
            this.toyRobotService.f = this.robotForm.get('f').value;
            this.toyRobotService.logs.splice(0, 0, `Placed robot at X: ${this.toyRobotService.x}, Y: ${this.toyRobotService.y}, D: ${this.toyRobotService.f}`);
        }
    }

    //move the robot
    move() {
        this.toyRobotService.performAction('MOVE');
    }

    //turn the robot left or right
    turn(direction) {
        this.toyRobotService.performAction(direction);        
    }

    //function to handle the REPORT button
    async report() {
        const alert = await this.alertController.create({
            header: 'Alert',
            message: 'This doesn\'t do anything at the moment because we already have a dynamic scrolling log of the results as the PDF says to implement.',
            buttons: ['OK']
        });

        await alert.present();
    }
}
