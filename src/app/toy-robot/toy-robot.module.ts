import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToyRobotPage } from './toy-robot.page';
import { ToyRobotPageRoutingModule } from './toy-robot-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule, ReactiveFormsModule,
    ToyRobotPageRoutingModule
  ],
  declarations: [ToyRobotPage]
})
export class ToyRobotPageModule {}
